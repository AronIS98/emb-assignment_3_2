
#ifndef FIFO_H
#define FIFO_H

const int FIFO_SIZE = 5;

class Fifo
{
public:
    Fifo();
    int get();
    void put(int item);
    bool is_empty();
    bool is_full();
    void reset();
    int circ_get();
    void circ_put(int item);
    bool circ_is_empty();
    bool circ_is_full();
    void circ_reset();
private:
    int buffer[FIFO_SIZE];
    int point_start;
    int point_end;
    int* pointer;
    int end;

    // add variables pointing to the front and back of the buffer
};

#endif // FIFO_H


